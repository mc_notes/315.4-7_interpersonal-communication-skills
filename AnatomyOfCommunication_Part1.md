# Anatomy of Communication:<br>How Understanding Can Transform Our Professional Interactions<br>Part One

> _A general introduction to some basic principles that help explain what makes conversations work well and what tends to get them into trouble._

_By Anita Simon and Ben E. Benjamin • Massage & Bodywork Magazine • February/March 2007_

## Contents <a id="contents"></a>
[Foundations of SAVI](#foundations-of-savi-) •
[Structure of SAVI](#structure-of-savi-) •
[Using the Rows in Conversation](#using-the-rows-in-conversation-) •
[Columns: Personal, Factual, Orienting](#columns-personal-factual-Orienting-) •
[Looking Ahead](#looking-ahead-) •
[Notes](#notes-) •
[References](#reverences-) •
[Resources](#resources-)

Language is frequently cited as the greatest intellectual achievement of the human race. We use language every day, for a wide range of purposes － from buying a newspaper to transacting a business deal to expressing our deepest feelings. In the field of massage therapy, our verbal communication has a tremendous impact on our clients, as well as on other professionals we encounter. Yet all too often, the ways we communicate don't quite work.

All of us at times have conversations that end badly, for reasons we don't fully comprehend. Even when we recognize our habitual patterns of communicating － noticing, perhaps, that we seem to have the same kind of arguments over and over again － we're often unable to change them. When we think we know what's going wrong, we frequently conclude that the problem lies with the other person. Not only is this conclusion usually wrong, but it leaves us helpless to improve the situation until that person changes somehow, which is not particularly likely.

Wouldn't it be great if we had an effective and objective way to analyze what's really going on in our communication － a method of examining a conversation as we would investigate an accident or a crime? Consider what happens when a motor vehicle accident occurs. At first, the only information we have is that which is immediately observable (the visible damage to the cars, any obvious injuries, etc.). Forensic investigators then use systematic methodologies to analyze the data and reconstruct what happened. They apply a scientific understanding of how things work to figure out where things started to go wrong. Then they retrace the steps that followed to determine what caused the damage and what could have been done to prevent it.

Imagine being able to do the same thing with communication. SAVI (the System for Analyzing Verbal Interaction) makes this possible.<sup>†</sup> After a difficult conversation, though we may be aware of what words were spoken and what emotional damage was caused, we often don't understand why things turned out badly. SAVI allows us to collect relevant pieces of evidence and use them to objectively and systematically analyze what happened. In contrast to an accident investigation, the pieces of evidence we examine with SAVI are not physical objects like skid marks or dented fenders, but communication behaviors.

Communication behaviors are specific, discrete types of verbal expression, such as Question, Answer, Opinion, Paraphrase, Gossip, Blame, and so on. A key concept in understanding communication is the distinction between _what_ we say (our content) and _how_ we say it (the verbal behaviors we use to communicate that content). Whether we communicate successfully depends more on the _how_ than on the _what_.

> **Key Concept:**   
> The make-or-break factors in communication are the verbal behaviors we use, not the content.

With this understanding of how communication works － how each type of verbal behavior tends to affect a conversation － we can pinpoint where things start to go wrong and devise strategies for achieving different outcomes. A major difference between forensics and communication analysis is that the goal of our investigation is not to find fault, but to find alternative ways of behaving that improve our chances of being understood.

In addition to providing insights after we've run into trouble, studying SAVI helps us prepare for future conversations to prevent problems from occurring in the first place. Again, consider the analogy to car travel. Engineers designing new roads don't wait for an accident to happen to determine where to paint white lines, where to build extra lanes, where to place traffic lights, and so on. Past experience and scientific analysis have led to practical guidelines. Similarly, with SAVI, building on information theory (note 1) has led to guidelines that can help us navigate more successfully in conversation. By learning about these guidelines, we can improve the ways we communicate with both clients and colleagues － as well as with our friends, family members, and other important people in our lives.

## Foundations of SAVI <a id="foundations-of-savi-"></a><sup>[▴](#contents)</sup>

In the sections that follow, we'll examine the general principles and reasoning that underlie the SAVI communication system.

**The Challenge of Communicating: Noise**

Before looking at how SAVI analyzes communication, it is helpful to clarify just what communication is. One of the simplest, broadest, and most useful definitions is "the transfer of information." SAVI focuses on verbal communication － the transfer of information through words and the associated voice tone.

Using this definition, communicating successfully means that a message gets transmitted accurately. The problem is that transmitting information accurately is not as simple as it may seem. Often the message that's received is quite different from the message that the speaker meant to send. This is because _how_ we say something (the communication behaviors we use) strongly affects _what_ the listener hears. Some behaviors, and patterns of behaviors, convey messages quite clearly; others, however, add "noise"—distracting or confusing interference. This noise acts like static on your radio or cell phone, making it more difficult for a message to get through. SAVI identifies three sources of noise: contradiction, ambiguity, and redundancy.

Often, noise is introduced through voice tone, when the tone contradicts the words. Voice tone is a very powerful, yet often overlooked, factor in the success or failure of a communication. You can't tell what communication behaviors people are using just by reading the words they've said. When said with different voice tones, the same words may constitute very different behaviors and, therefore, have widely varying effects on a conversation. For example, consider the statement, "I am not angry." The effect of these words will be quite different depending on the voice tone that's used － whether the person is stating, "I am not angry" in a calm, neutral manner or screaming, "I am NOT angry!!!" The latter is a clear example of contradiction: while the words say, "I am not angry," the tone says, "I am absolutely furious!"

Contradiction is a major source of noise in communication. An example of a communication behavior that introduces contradiction is Sarcasm. Consider an interaction in which a massage therapist is talking to a client about the importance of starting sessions on time. After the therapist says, "You've been late three times this month," the client responds in a sarcastic tone, "Oh, right, and you're always on time." Here the conflicting messages are: "You're always on time" (words) and "You're not always on time!" (tone). Whenever two contradictory messages are sent at the same time － one through words, and a different one through tone － it is significantly less likely that information will get through and be available for solving problems or making decisions.

Contradiction between words and tone isn't the only way noise creeps into our messages. Information also gets lost when we make comments that are ambiguous. For instance, if your client tells you, "Your office decor is very unusual!" or "You have an interesting style of working," it's not clear whether the client is criticizing you or paying you a compliment. This ambiguity makes it less likely that you'll understand what was really meant and respond appropriately.  

The third source of noise in our communication is redundancy － being so repetitious that the listener tunes out. An understanding of the impact of noise goes a long way toward knowing why some conversations work well and others do not.

> **Key Concept:**   
> Noise = Contradiction, ambiguity, and/or redundancy  
> _Noise blocks the transfer of information._

**Understanding SAVI: the shift from analyzing people to analyzing behavior**

When people try to figure out why a conversation went badly, they often refer to the personal characteristics of the other person － their motivations, hidden agendas, personality traits, and so forth. This involves a great deal of speculation, as it is difficult to know what someone else is thinking or feeling. In contrast, SAVI focuses on the actual observable behaviors people use as they communicate. This descriptive approach makes the job of analyzing, understanding, and intervening in difficult conversations a whole lot easier.

## Structure of SAVI <a id="structure-of-savi-"></a><sup>[▴](#contents)</sup>

The SAVI Grid identifies various distinct types of communication behaviors (see Figure 1). These behaviors (called _categories_) fit into nine squares arranged in the grid below. The sections that follow discuss the Rows and Columns of the SAVI Grid in more detail.

![Figure 1. SAVI Grid by Anita Simon & Yvonne Agazarian, 2006.](AnatomyOfCommunication_Part1_files/Fig1_SAVIGrid.jpg)

**The Rows: Red, Green, and Yellow**

We use the metaphor of a stop light to talk about the three SAVI Rows. The color of each row suggests its impact on the flow of information.

_**Red light**_ behaviors tend to hinder the transfer of information. They maximize noise in the communication.

Let's consider how Red light behaviors may relate to conversations that are going badly. Think about a recent dialogue that didn't work well, and try to recall the types of things that were said. Now, take a look at the Red light row, and see if you find any matches to the behaviors in your own challenging interaction. Did you find some? When we ask this question in SAVI workshops, most people recognize that in their difficult conversations they Blame others, Yes-But, Complain, use Sarcasm, or change the subject by Joking around. A conversation that is upsetting is more likely than not to contain Red light behaviors.

_**Green light**_ behaviors give evidence that information has been transferred. They minimize noise in a communication.

Most people report that these behaviors grease the wheels of communication, enabling information to flow back and forth more easily. Examples of Green light behavior in a conversation are Answering questions, Paraphrasing messages to check for accurate understanding, Building upon proposals instead of dropping them, explicitly Agreeing with or valuing ideas, Summarizing opinions, and expressing feelings and wants in a non-blameful, non-complaining manner. The behaviors in the Green light row are necessary to help increase morale and productivity.

_**Yellow light**_ behaviors introduce unsolicited information into a conversation.

We use Yellow light behaviors to give information and nonjudgmental opinions or to solicit information from others. When you look at this row, you'll see that this is where we give our Facts and our Opinions, make our Proposals, talk about our preferences, and ask others for Facts or for their Opinions, wants, or ideas.  

The effect of Yellow light behaviors on the transfer of information is a bit tricky to explain. It's important to recognize that _introducing_ information is not the same as _transferring_ it to someone else － and _soliciting_ information doesn't mean that we'll get an answer. In other words, _talking_ in the Yellow light row is different from _communicating_. Sometimes you may think you're communicating, when actually you're just talking to yourself, even though there are others in the room who look like they're listening to you.

A typical situation where conversation consists primarily of Yellow light behaviors is an unproductive staff meeting, in which each person gives new information but nobody responds to anyone else's input. Each time new Facts, Opinions, Proposals, or Questions are introduced they seem to disappear like stones thrown in a lake, without leaving a ripple. At the end of such meetings, those involved have no evidence that anyone else understood what they said, let alone whether they agreed or disagreed. This Yellow light pattern is why so many meetings feel so frustrating. It may often feel as though they're going in circles － and from a SAVI Grid standpoint, that's exactly what's happening; the conversation is going around and around in Yellow light (see Figure 2).

![Figure 2 - Yellow Light Cycles](AnatomyOfCommunication_Part1_files/Fig2_YellowLightCycles.png)

When stuck in such a frustrating situation, many people's first instinct is to introduce even more Facts, Opinions, and Proposals, or to repeat what they've already said more loudly. However, when a communication system is already overloaded, putting in more information doesn't help; it just continues to feed the repetitive Yellow light cycle. What _does_ help is to make use of what's already on the table by shifting to communication behaviors that process that information － that is, Green light behaviors. For participants in a staff meeting, such behaviors might include: Answering questions that have previously been asked (Figure 3); Building on someone else's ideas (Figure 4); or Paraphrasing or Summarizing what's been said so far (Figure 5). (See: Moving From Yellow Light to Green Light.)

![](AnatomyOfCommunication_Part1_files/Fig3-5_MoveYellowToGreen.jpg)

A Yellow-Green information climate increases the chances that noise will be reduced and that new information will be transferred and processed. In a staff meeting, this means that the participants are much more likely to meet their goals and solve the problems they're trying to tackle.

Here we've seen that giving information is no guarantee that it will be used productively. In an all-Yellow communication climate, it doesn't get used at all. In other cases, Yellow light information _does_ get used, but in a way that's counterproductive. For instance, in a Yellow-Red climate, your information may just provide more ammunition to fight with. This is what's happening when you find that everything you say gets Yes-Butted, Discounted, Attacked, or Complained about, or that the subject gets changed by Joking around or Gossip. In that type of communication climate, giving more Facts, Opinions, or Proposals is more likely to provoke competition or start an argument than to help in problem solving. For problems to get solved, some Green light behaviors need to be used － and probably by you, if you're the only one in the conversation with the skills to do that. It's within a Yellow-Green communication climate that Yellow light information is used to make decisions and generate creative solutions.

A key takeaway message here is that _whenever you use a Yellow light behavior, it's the behaviors that follow which help determine the likelihood that your information will be used productively._ If you're trying to get new information across, watch carefully to see which types of behaviors follow your input. If they are part of a Yellow-Green sequence, it's safe to continue. If they are Yellow-Yellow or Yellow-Red, it probably won't be helpful to bring more new facts or ideas into the conversation.

> **Key Concept:**  
> It's the behaviors that follow what you say which indicate whether that input will be used or opposed.

## Using the Rows in Conversation <a id="using-the-rows-in-conversation-"></a><sup>[▴](#contents)</sup>

To highlight the distinctions between the three rows, let's look at how the same topic might be discussed in each row. Take the example of a massage therapist who's talking with a colleague about a client who doesn't seem to be getting any better. The colleague may respond with Red light, Yellow light, or Green light behaviors. As you read through the various responses listed in the box below, think about how you'd feel if you were on the receiving end of each of these comments:

> **Massage therapist says: "I'm feeling upset and at a loss － my client has felt no relief after several sessions."**
> 
> _**Red Light Responses:**_
> 
> _Attack._ "You must have missed something important."  
> _Discount._ "Don't worry about it."  
> _Yes/But._ "Yeah, but most of your clients are doing great."
> 
> _**Yellow Light Responses:**_
> 
> _Personal Information Past._ "The last time this happened to me, I referred the client to another practitioner."  
> _Broad Question._ "What have you tried? What do you think is the main problem?"  
> _Proposal._ "You could try calling one of your old teachers for a consult."  
> 
> _**Green Light Responses:**_
> 
> _Paraphrase._ "I'm hearing that you're very concerned about this client because although you've worked with him for several sessions, he's not feeling better."  
> _Feeling question._ "Are you worried that you're missing something, or are you upset because you think the problem is untreatable?"

## Columns: Personal, Factual, Orienting <a id="columns-personal-factual-Orienting-"></a><sup>[▴](#contents)</sup>

So far we've been looking at the Rows of the Grid; now let's switch to thinking about the Columns. There are three Columns in the Grid, and these reflect the focus of the information carried in the communication.

_**Column 1: Personal**_ behaviors － focus on the person part of the message (see Figure 6).

Column 1 contains Personal behaviors. These are the behaviors we use to talk about ourselves, invite our listeners to talk about themselves, talk about our relationship, or express our feelings and impulses. The primary information they give is about the emotional state of the person speaking or the relationship between the people having the conversation. As we've seen from the discussion of the Rows, you can express Personal information using behaviors in the Red, Yellow, or Green light rows, and these will yield quite different effects.

> Personal behaviors are the behaviors we use to talk about ourselves, invite our listeners to talk about themselves, talk about our relationship, or express our feelings and impulses.

For example, if you're feeling angry, you could express that emotion directly:

* "I'm angry at my client for not showing up and not calling." (Square 7—Inner-feeling)  
* Or you can express it indirectly: "That client is such a jerk." (Square 1—Attack)  
* You can also express your wants or preferences directly: "I want to work no more than four five-hour shifts a week." (Square 4—Personal information current)  
* Or, again, you can express it indirectly: "How do they expect us to keep working at this pace, doing so many shifts?" (Square 1—Complaint)

![Figure 6. Column 1. Personal](AnatomyOfCommunication_Part1_files/Fig6_C1Personal.jpg)

_**Column 2: Factual**_ － focus on data (see Figure 7).

The behaviors in Column 2 relate to factual information that may be needed to solve problems. We use Factual behaviors to process, provide, or avoid concrete information about the world. 

> Factual behaviors are used to process, provide, or avoid concrete information about the world.

One of the most common ways we avoid getting factual data is by giving Negative predictions (Square 2): 

* "Once you hear the topic of tonight's lecture, you're not going to want to come with me."


We can get information into the conversation by introducing Facts and asking Questions (Square 5): 

* "Tonight's lecture is by Dr. Jones. He's going to talk about the effectiveness of hands-on energy work. Have you heard about his research?"  

Once we've been given Facts or asked Questions, we can process that information by giving direct responses (Square 8): 

* "Yes, I have. I've read his latest book." (Answering a question)  

![Figure 7. Column 2. Factual](AnatomyOfCommunication_Part1_files/Fig7_C2Factual.jpg)

_**Column 3: Orienting**_ － focus on directing the flow of what is being talked about (see Figure 8).

Column 3 behaviors influence the direction in which a conversation is moving. They orient listeners toward or away from the current discussion or change the subject by giving new Opinions or Proposals.

> Orienting behaviors influence the direction in which a conversation is moving

When we introduce new topics, it's often in the form of Opinions (Square 6): "I think all health insurance plans should cover massage therapy."

Once a topic has been introduced, we can orient toward that topic － for example by expressing Agreement and Building (Square 9) on a previously mentioned idea: "I agree. That would make massage accessible to many people who couldn't otherwise afford it."  

Alternatively, we can orient away from that topic. This is frequently done in the form of a token agreement followed by a different idea (a "Yes-But," Square 3): "Yes, but then the insurance companies could tell us what we can charge, like they do with physical therapists."

![Figure 8. Column 3. Orienting](AnatomyOfCommunication_Part1_files/Fig8_C3Orienting.jpg)

## Looking Ahead <a id="looking-ahead-"></a><sup>[▴](#contents)</sup>

So far, we've given you a general introduction to some basic principles that help explain what makes conversations work well and what tends to get them into trouble. You've learned about the conceptual foundations of SAVI, and you've started to get a sense of how the SAVI Grid works. In part two of this article, we'll discuss how the Rows and Columns of the Grid come together, with an in-depth look at each of the nine Squares. We'll then show how you can use all this information to recognize and change habitual, unconstructive patterns of communicating and to help keep your professional conversations from going offtrack. If you're eager to learn more and don't want to wait for the next issue, you can read the conclusion of this article online at [www.massageandbodywork.com](http://www.massageandbodywork.com/).

† SAVI is a registered trademark of Anita Simon and Yvonne Agazarian.

* Anita Simon, EdD, is codeveloper of SAVI with Yvonne Agazarian. She has been writing about SAVI since 1965 and has delivered workshops on this and related topics since 1968. Simon is in private practice as a psychotherapist in Philadelphia, specializing in work with couples, business partners, and individuals. She can be contacted at <anitasimon@savicommunications.com>. The SAVI website is <a href="http://www.savicommunications.com/" target="_blank">www.savicommunications.com</a>.*

* Ben E. Benjamin, PhD, holds a doctorate in education and sports medicine. He is senior vice president of strategic development for Cortiva Education and founder of the Muscular Therapy Institute. Benjamin has been in private practice for more than forty years and has taught communications as a trainer and coach for more than twenty-five years. He teaches extensively throughout the country on topics including communication, SAVI, ethics, and orthopedic massage, and is the author of* Listen to Your Pain, Are You Tense? and Exercise without Injury and *coauthor of* The Ethics of Touch. *He can be contacted at <bbenjamin@cortiva.com>.*

## Notes <a id="notes-"></a><sup>[▴](#contents)</sup>

1. SAVI was developed in the 1960s by Anita Simon and Yvonne Agazarian (Agazarian 1968, Simon & Agazarian 2000) as a tool to study the effects of verbal behavior in educational, therapeutic, and organizational settings. The groundbreaking theory of information transfer had originally been investigated by Claude Shannon at Bell Labs (Shannon & Weaver, 1964) to determine what got in the way of accurately transmitting electronic messages in Morse code. Yvonne Agazarian (2000) adapted and modified that work to apply to human verbal communication.

## References <a id="reverences-"></a><sup>[▴](#contents)</sup>

* `1964` Shannon, Claude E., & Weaver, W. The Mathematical Theory of Communication. Urbana, IL: University of Illinois Press. 
    * Information transfer theory was investigated at Bell Labs to determine what got in the way of accurately transmitting electronic messages in Morse code.
* `1968` Agazarian, Y. M. A Theory of Verbal Behavior and Information Transfer. Dissertation submitted at Temple University, Philadelphia.  
* `2000` Agazarian, Y. M & Gantt, S.P. "Autobiography of a Theory: Developing a Theory of Living Human Systems and its Systems-Centered Practice" Jessica Kingsley Publishers Ltd, London.
* `2000` Simon Anita, & Agazarian, Y. M. "SAVI—the System for Analyzing Verbal Interaction," in The Process of Group Psychotherapy: Systems for Analyzing Change, Beck, A. P. & Lewis, C. M. (Eds), Washington, D.C.: American Psychological Association, 357–80.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* Message & Bodywork Magazine (Wayback Machine)
    * [The Anatomy of Communication, Part One (FebMar2007 PDF)](https://web.archive.org/web/2013*/http://www.massageandbodywork.com/Articles/FebMar2007/EssentialSkillsPart11.pdf)
    * [The Anatomy of Communication, Part II (AprMay2007 PDF)](https://web.archive.org/web/20110217033058/http://www.massageandbodywork.com/Articles/AprilMay2007/EssentialSkillsPart%202.pdf)
* MessageTherapy.com
    * [The Anatomy of Communication, Part One (HTML)](https://www.massagetherapy.com/articles/anatomy-communication-how-understanding-can-transform-our-professional-interactions) from Massage & Bodywork magazine, February/March 2007
* SAVI Communications: [The SAVI® Grid](http://savicommunications.com/SAVI_grid.html)
* Wikipedia
    * [Systems-centered therapy](https://en.wikipedia.org/wiki/Systems-centered_therapy)

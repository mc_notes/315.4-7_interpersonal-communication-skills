# Anatomy of Communication:<br>How Understanding Can Transform Our Professional Interactions<br>Part Two

_By Anita Simon and Ben E. Benjamin • Massage & Bodywork Magazine • February/March 2007_

## Contents <a id="contents"></a>
[The Squares](#the-squares-) •
[Squares in Action](#squares-in-action-) •
[Dynamic Systems Perspective](#dynamic-systems-perspective-) •
[Common Redundant Noise Patterns](#common-redundant-noise-patterns-) •
[SAVI in Practice](#savi-in-practice-) •
[About The Authors](#about-the-authors-) •
[References](#references-) •
[Resources](#resources-)

In Part One of Square 1 behaviors this article, we discussed how important it is for massage therapists to communicate effectively, and we looked at just how challenging that can be. Often, even after an interaction is over, it's difficult to understand why it went well or went badly. We introduced SAVI (the System for Analyzing Verbal Interaction) as a useful tool for pinpointing what goes wrong in our conversations and devising strategies to get them back on track. So far we've discussed the general principles of SAVI and some specifics of the SAVI Grid, including an in-depth look at the Rows (Red, Yellow, and Green) and Columns (Personal, Factual, and Orienting). Here in Part Two, we'll finish our tour of the Grid by looking at each of the nine Squares. Then we'll bring all this information together to explore how we can apply SAVI skills to real-life conversations, both in and out of the treatment room.

## The Squares <a id="the-squares-"></a><sup>[▴](#contents)</sup>

The impact of the Rows and Columns on our communication becomes clearer when we see how they intersect in the nine Squares. For example, consider the intersection of Column 1 (Personal) with Row 1 (Red Light). If you think about a bad argument you've had with someone close to you, you'll probably have firsthand knowledge about the behaviors that belong in this Square. Square 1's label, "Fighting," says it all. [Figure 12]

Square 1 behaviors such as Attack, Self-Attack, Sarcasm, and Complaint all belong in the Personal column because they give very important information about the speaker's feelings. Think back to the argument you had.

Chances are, you were both talking about something that provoked strong feelings in you. The problem with Square 1 is that the emotions in the message are conveyed _indirectly_. For instance, we may express anger through Attack ("You're so irresponsible when you borrow my things"); sadness or frustration through Self-Attack ("I'm just lousy at marketing myself"); or apprehension through Complaining ("Integrating new therapists into our practice is always so stressful") and so forth. These important emotional messages can be transferred with less noise by being direct, in Square 7 (e.g., "I'm annoyed that you didn't return my massage table when you said you would"; "I'm feeling discouraged because I only got two responses from my last ad"; or "When I think about this new person joining our practice I feel nervous." Expressing our feelings directly makes it much more likely that our listener will hear what we're trying to say and respond effectively to our concerns.

> Often, even after an interaction is over, it's difficult to understand why it went well or went badly.

Each of the Squares has its own distinctive character. To illustrate, let's look at some concrete examples of behaviors from each one. All the examples address the same situation: two practitioners debating whether a massage therapist's choice of work clothes should be governed more by comfort and freedom of movement or by the desire to present a professional appearance. Let's see how comments on this topic would sound from all nine Squares.

_**Square 1: Fighting**_

_The behaviors of this square are put-downs of the self or others through content or voice tone._

* Attack: "That tank top you wore yesterday was totally unprofessional." 
* Righteous Question: "Don't you have any sense of how a healthcare professional should dress?!"

_**Square 2: Obscuring**_

_The behaviors of this square include assumptions presented as fact; statements that are ambiguous in topic, content, or source; and stories or jokes that distract (for better or worse) away from the flow of a conversation._

* Gossip: "Did you hear that Paul got fired on his first day because he refused to wear the uniform?" 
* Positive Prediction: "In 10 years, massage therapists will feel they have the freedom to dress however they want to." 
* Mind-Reading: "I just know my clients think I dress really poorly."

_**Square 3: Competing**_

_Square 3 behaviors express opinions that directly or indirectly contradict or interrupt the input of others._

* Yes-But: "It's all well and good to want comfort, but massage therapists need to look professional if they want to be treated as professionals."
* Leading Question: "Don't you think we'd get more respect if we dressed more professionally?"

_**Square 4: Individualizing**_

_Square 4 behaviors express information, opinions, or questions about one's social self that are not deeply personal._

* Personal Information Past: "When I first started as a massage therapist, I found one style of shirt that I liked and bought ten of them."
* Personal Question: "What kind of clothing are you most comfortable working in?"

_**Square 5: Finding Facts**_

_Square 5 behaviors either give information verifiable by observation and public data or ask questions that seek such information from others._

* Facts & Figures: "According to one survey, 78 percent of massage therapists feel a professional image is important for generating client respect."
* Narrow Question: "Do most of your colleagues dress for comfort or for professional image?"

_**Square 6: Influencing**_

_Square 6 behaviors convey the speaker's opinions and orientation about what is being discussed._

* Opinion: "I think wearing white culottes with white tops would be best." 
* Proposal: "Let's do a survey of our clients and see what they think about our office attire."

_**Square 7: Resonating**_

_Square 7 behaviors tune in to information that is emotionally meaningful to the speaker or to others. They frequently involve a sense of taking a risk or going out on a limb._

* Feeling Question: "Were you upset by that comment I made earlier about your tank top?"
* Answers Feeling Question: "Yes, I was really upset when you said that."

_**Square 8: Responding**_

_Square 8 behaviors express direct responses to others' input or otherwise give evidence that input has been processed or received._

* Answer Question: "In answer to your question, I think it's more important that a massage therapist look professional than be dressed comfortably."
* Paraphrase: "What I heard you say is that looking professional is more important to you than your personal comfort."

_**Square 9: Integrating**_

_Square 9 behaviors are cooperative acts that integrate by explicitly valuing or building upon someone else's communication._

* Agreement: "I agree with what you said earlier— what we wear has a direct impact on how our clients perceive us."
* Positives: "I like what you're wearing right now. It looks comfortable and it looks professional."

## Squares in Action <a id="squares-in-action-"></a><sup>[▴](#contents)</sup>

**Squares in Action: Building a Yellow-Green Climate**

Imagine that a client tells his therapist that his friend's therapist has been using myofascial therapy, and the following conversation takes place:

|||
|--------:|:--------------------------------------------------------------------|
| Client: | "Don't you think I might be getting better faster if you were doing myofascial therapy with me?"  
|Therapist: | _(said with irritation)_ "If I thought it would be better for you, don't you think I'd be using it?"

Here's what happened:

<table style="width:100%; font-family: monospace;">
<tr>
  <td></th>
  <td style="text-align:right;">Client:</th>
  <td>"Don't you think I might be getting better faster if you were doing myofascial therapy with me?" (Leading Question)</th>
</tr>
<tr>
  <td></td>
  <td style="text-align:right;">Therapist:</td>
  <td>"If I thought it would be better for you, don't you think I'd be using it?" (Righteous Question)</td>
</tr>
</table>

If the therapist thought back on this interaction, she could notice that her client's comment was a Leading question, a Red light behavior, and she might remember the pull she felt to join the client up in Red. The next time a client asks a question of the "Don't you think ..." variety, the therapist can choose among various Yellow or Green light alternatives. For example:

_Expressing Agreement (Square 9):_  

* "I'm glad when you feel free to ask me questions about something that is of concern to you."

_Giving a Paraphrase (Square 8) and checking it out with a Narrow question (Square 5):_  

* "I'm hearing you tell me that your friend's therapist is using myofascial therapy and you're wondering whether that treatment would be helpful for you, too. Is that right?"

_Asking a Broad question (Square 5):_  

* "Why are you thinking that myofascial therapy is good for you?"

_Asking a Personal question (Square 4):_  

* "Is there something about your treatment that is concerning you?"

## Dynamic Systems Perspective <a id="dynamic-systems-perspective-"></a><sup>[▴](#contents)</sup>

**Putting it All Together: A Dynamic Systems Perspective**

Until now we've been talking about communication by focusing on specific, isolated behaviors: Green light behaviors that carry a minimum of noise, Red light behaviors that carry lots of noise, and Yellow light behaviors that bring in unsolicited information.

> Communication is like a chess game: we can't understand what's happening by looking at an individual "move."

However, since every conversation is a dynamic system, no single behavior can determine how that conversation will evolve. Communication is like a chess game: we can't understand what's happening by looking at an individual "move." For example, a pawn moved onto a certain square may be a winning move in one game and a losing move in another. In chess, we need to examine the sequence of moves to get a sense of what's going on. Looking at the big picture is essential in determining how to make a useful next move. Likewise, to understand why a conversation is going well or going poorly, we need to look at the sequence of communication behaviors. Analyzing the sequence of behaviors is the direct route to making useful decisions about what behaviors to use next.

Looking at a conversation as a dynamic system, it becomes clear that no particular communication behavior is good or bad in and of itself; the effect of any behavior depends on the sequence of interactions that have been happening (which we've called the "communication climate"). As we saw in the section on Yellow light behavior, in Yellow-Green communication climates, new information contributes usefully to problem solving and decision making, but in Red-Yellow climates, that same input can make the problem worse. Consider, for example, the situation in which a colleague has been complaining about his working conditions for months. You've tried to be helpful by listening and making suggestions (Proposals), and every time his response is to complain more about why your proposal won't work.

|||
|-----:|:--------------------------------------------------------------------|
|_Colleague:_| It's so dark in here that it's hard to give a treatment! (Complaint)  
|      _You:_| Let's bring in a lamp. (Proposal)  
|_Colleague:_| That will make it too crowded in here. (Complaint)  
|      _You:_| Well, let's ask for a light fixture. (Proposal)  
|_Colleague:_| They'll never approve of that and we'll be in the dog house. (Complaint)  
|      _You:_| Well, let's...etc. etc. (Proposal)  

It's clear that this dialogue is going nowhere. So what's the problem? In a word: _redundancy_. In Part One of this article, we talked about redundancy as the third source of noise in a communication system (in addition to contradiction and ambiguity). Redundancy makes it more difficult for a message to get through because we tune out when we hear the same thing repeated over and over again. We intuitively know that redundancy makes us tune out when it is the _**content** that gets repeated_; we get bored hearing about someone's vacation for the umpteenth time. What may be less obvious is that a repetitious _pattern of communication_ behaviors can also be redundant and cause us to stop listening. The "It's dark in here" dialogue illustrates how we can get caught in a spin cycle of repeating behaviors: [Figure 24]

![Fig24 Complaint Proposal Cycle](AnatomyOfCommunication_Part2_files/Fig24_ComplaintProposalCycle.png)

```
Behavior A is followed by
behavior B followed by
behavior A followed by
behavior B, etc.
```

No matter what person A says, person B will respond predictably, and then A will try again with the same behavior, maybe louder, only to be met by B's typical response.

From a SAVI perspective, there's nothing inherently wrong with a single use of any behavior. What's causing the problem in our dialogue is not the use of a Complaint or a Proposal; rather, it is the ineffective jointly created pattern of redundant verbal behaviors (Complaint/Proposal/Complaint/Proposal, etc.).

## Common Redundant Noise Patterns <a id="common-redundant-noise-patterns-"></a><sup>[▴](#contents)</sup>

**Examples of other common noisy, redundant patterns are:**

_**Yes-But/Yes-But/Yes-But**_

|||
|-------:|:--------------------------------------------------------------------|
| Therapist: | Do these exercises three times a day.
|    Client: | But I don't have that kind of time.
| Therapist: | I understand, but if you don't do the exercises, you will heal much more slowly.
|    Client: | Yes, but I already work sixteen hours a day.
| Therapist: | But if you don't get better, you won't be able to work at all for quite some time.
|    Client: | But...

Try putting yourself in the place of either the client or the therapist. How does it feel to be involved in this redundant pattern? Can you predict how the conversation might proceed from there?

_**Narrow Question/Answer/Narrow Question/Answer**_

|||
|--------:|:-------------------------------------------------------------------|
| Therapist: | How often did you do your exercises this week?
|    Client: | Once a day.
| Therapist: | Can you increase that to three times a day?
|    Client: | No,not really.
| Therapist: | What can you drop out of your schedule to allow more time to exercise?
|    Client: | Nothing–I'm really busy right now.
|         …  | … etc.

If you were the client in this dialogue, how would you be feeling toward the therapist at this point? How would you feel if this pattern of questions and responses were to continue for several more minutes?

_**Question/Non-Answer/Question/Non-Answer**_

|||
|--------:|:-------------------------------------------------------------------|
|    Client: | When will I start to feel better?
| Therapist: | Well, you know these things take time.
|    Client: | Can you give me a range?
| Therapist: | You know, if I had a nickel for every time I've been asked that question, I'd be rich.
|    Client: | Well, are we talking weeks or months or years?
|         …  | … etc.

Again, put yourself in the client's place. What could the client do differently when the therapist didn't answer the question for a second time? Consider how the redundant pattern might change if the client switched to using Personal information current, Square 4, and Narrow Question, Square 5: "I notice you're not answering my question. Is there a reason?" Likewise, putting yourself in the therapist's place, think about how you'd feel inside this redundant loop, and then compare that with how you'd feel if you switched behaviors (e.g., "You're asking me a question that I don't have an answer for. That's frustrating to me because I'd like to be able to give you a real number.").

_**Attack/Attack/Attack**_

The therapist goes home after a hard day, and his spouse meets him at the door and says:

|||
|-------:|:--------------------------------------------------------------------|
|    Spouse: | Where have you been?! We're late!  
| Therapist: | Don't start again. I've had a rotten day.  
|    Spouse: | It's always been about you!  
| Therapist: | You don't even care that we may lose our lease and I was trying to do something about it.  
|    Spouse: | Everything's more important to you than I am.
| Therapist: | You just don't appreciate how much I do for you.  

It's clear that some strong negative emotions are coming up for both spouses. If this Red light, indirect pattern of discharging their emotions were to continue, would you expect them to feel better or worse?

> Even in habitual, repeated conversations that always seem to end in a stalemate, one person can help move the conversation toward a satisfying outcome for both by altering his or her own side of the dialogue.

The good thing about spotting redundant patterns is that it gives us a chance to change them. Since we're talking about sequences of behaviors, noisy redundant patterns can be "fixed" by anyone in the conversation by means of trying a different behavior. Let's see how this might happen. Think back to the Complaint/Proposal dialogue. In this example, either speaker could interrupt the redundant pattern by trying a different sort of behavior. For instance, you (the person giving Proposals) could switch to:

* Asking a Personal question, Square 4 (e.g., "What do you think we ought to do?" or "What kind of help would you like from me?") or
* Giving a Command, Square 6 (e.g., "Tell me what you think might help.")
* Or your colleague (the person Complaining) could switch to active problem solving by making a Proposal of his own:
    * "I'm going to ask the staff to take five minutes to brainstorm about improving the lighting."

As we realize that it's patterns (sequences of behaviors)—not just individual behaviors—that make or break conversations, it also becomes clear that no individual ever has sole responsibility for the failure of a communication. Even if one person uses noisy behaviors such as Attack, Sarcasm, or Yes-Buts, that alone does not cause a conversation to fail. In any dialogue, patterns of communication evolve through input from both parties, so both have the ability to influence how things are going. Even in habitual, repeated conversations that always seem to end in a stalemate, one person can help move the conversation toward a satisfying outcome for both by altering his or her own side of the dialogue. This empowering realization is at the heart of SAVI teaching.

## SAVI in Practice <a id="savi-in-practice-"></a><sup>[▴](#contents)</sup>

At this point you've learned a lot about the SAVI Grid, from its theoretical underpinnings to the nuts and bolts of rows, columns, and squares and how they interact in dynamic conversations.

"So what?" you may be tempted to ask. "I can't possibly track every single behavior I use!" And you'd be absolutely right. The good news is that SAVI is not meant to be used all the time. But, like a hammer or a paint brush, it's a great tool to have on hand for those occasions when you need it. The most useful time to "think SAVI," that is, to think about the verbal behaviors you're using, is when you're in a conversation that makes you uncomfortable or that doesn't seem to be going well. For example, if you're in the midst of in a heated argument and realize you need to take a break to calm down, you can use SAVI instead of just counting to ten. Often, just spotting the color of the communication behaviors being used enables you to start figuring out what's going wrong and how to make things work better. Even if you can't take a break in the middle of the conversation, you can try to find time in the hours or days that follow to consider what you might do differently in the future.

Since our communication habits are deeply ingrained, we may
have difficulty recognizing our own communication blind spots. If you are lucky enough to know someone you can talk these ideas over with, by all means use him or her as a resource. The people we live, work, and play with often know more about our behaviors than we do. They may have feedback and suggestions that would not have occurred to us.

If you do invite feedback, you may be surprised by what you learn. One person who took a SAVI workshop brought the ideas home to share with his family, and put a SAVI Grid up on the refrigerator. From then on, his kids pointed out whenever he was "Yes-Butting" them, which he was doing much more than he had realized. In exchange for putting up with his own behavior being monitored, he had a tool to help deal better with his children's "You did"/"No I didn't"/"Yes, you did"/"No I didn't" never-ending battles.

In future articles, we'll explore specific communication behaviors, and patterns of behavior, in much greater detail. We'll also translate this knowledge into action, with practical strategies for noticing when conversations are going off-track and getting them moving in a more constructive direction. In our own lives, when we're navigating challenging interactions, we've found that our in-depth understanding of communication has acted like a life preserver—something we can hold on to that keeps us afloat. We hope you'll find this exploration of the anatomy of communication useful in enhancing and enriching your own life and work. `M&B`

## About The Authors <a id="about-the-authors-"></a><sup>[▴](#contents)</sup>

Anita Simon, EdD, is co-developer of SAVI with Yvonne Agazarian.  
She has been writing about SAVI since 1965 and has delivered workshops on this and related topics since 1968. Simon teaches SAVI regularly at a variety of professional conferences in the fields of psychotherapy, human resources, and education. She also maintains a private psychotherapy practice in Philadelphia, specializing in work with couples, business partners, and individuals.

Ben E. Benjamin, PhD, holds a doctorate in education and sports medicine.  
He is senior vice president of strategic development for Cortiva Education and founder of the Muscular Therapy Institute. Dr. Benjamin has been in private practice for more than forty years and has taught communications as a trainer and coach for more than twenty-five years. He teaches extensively throughout the country on topics including communication, SAVI, ethics, and orthopedic massage, and is the author of Listen to Your Pain, Are You Tense? and Exercise without Injury and coauthor of The Ethics of Touch. He can be contacted at bbenjamin@cortiva.com.

## References <a id="references-"></a><sup>[▴](#contents)</sup>

* `1964` Shannon, Claude E., & Weaver, W. The Mathematical Theory of Communication. Urbana, IL: University of Illinois Press. 
    * They investigated information transfer theory at Bell Labs to determine what got in the way of accurately transmitting electronic messages in Morse code.
* `1968` Agazarian, Y. M. A Theory of Verbal Behavior and Information Transfer. Dissertation submitted at Temple University, Philadelphia.  
* `2000` Agazarian, Y. M & Gantt, S.P. "Autobiography of a Theory: Developing a Theory of Living Human Systems and its Systems-Centered Practice" Jessica Kingsley Publishers Ltd, London.
* `2000` Simon Anita, & Agazarian, Y. M. "SAVI—the System for Analyzing Verbal Interaction," in The Process of Group Psychotherapy: Systems for Analyzing Change, Beck, A. P. & Lewis, C. M. (Eds), Washington, D.C.: American Psychological Association, 357–80.

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* Message & Bodywork Magazine (Wayback Machine)
    * [The Anatomy of Communication, Part One (FebMar2007 PDF)](https://web.archive.org/web/2013*/http://www.massageandbodywork.com/Articles/FebMar2007/EssentialSkillsPart11.pdf)
    * [The Anatomy of Communication, Part II (AprMay2007 PDF)](https://web.archive.org/web/20110217033058/http://www.massageandbodywork.com/Articles/AprilMay2007/EssentialSkillsPart%202.pdf)
* MessageTherapy.com
    * [The Anatomy of Communication, Part One (HTML)](https://www.massagetherapy.com/articles/anatomy-communication-how-understanding-can-transform-our-professional-interactions) from Massage & Bodywork magazine, February/March 2007
* [Savi Communications: The SAVI® Grid](http://savicommunications.com/SAVI_grid.html)
* Wikipedia
    * [Systems-centered therapy](https://en.wikipedia.org/wiki/Systems-centered_therapy)
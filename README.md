# `315.4/.7` Interpersonal Communication Skills

_The "Anatomy of Communication" articles discuss the **System for Analyzing Verbal Interaction (SAVI)** principles which can help explain what makes conversations work well, what tends to get conversations into trouble, and applied approaches to improve communication skills._

**Anatomy of Communications, How Understanding Can Transform Our Professional Interactions**

[Part One (markdown format)](AnatomyOfCommunication_Part1.md)

* Introduces some basic principles that help explain what makes conversations work well and what tends to get conversations into trouble.
* Provides a conceptual foundations for SAVI and the SAVI Grid.

[Part Two (markdown format)](AnatomyOfCommunication_Part2.md)

* Discuses how the SAVI Grid Rows and Columns come together.
* Covers each of the nine Grid Squares.
* Shows how to recognize and change habitual, unconstructive communication patterns.
* Provides approaches to help keep professional conversations from going offtrack.

**SAVI Grid**

> The SAVI Grid is a visual map of communication used by organizational consultants, therapists, educators and many others interested in understanding and improving the interactions in their personal and professional lives.
> 
> The rows of the SAVI Grid are based on information theory as framed by Shannon and Weaver, and on the principle that communication behavior either _approaches or avoids solving the problems inherent in information transfer_ (derived from Howard and Scott). The Classic SAVI Grid uses the language of theory to label the rows: Avoidance, Contingent, Approach. The Traffic Light SAVI Grid uses a traffic light metaphor to convey the idea of approaching or avoiding information transfer: Red Light for Avoidance, Yellow Light for Contingent, Green Light for Approach.

![](README_files/SAVI_Classic_Grid_2018.png)

## Resources <a id="resources-"></a><sup>[▴](#contents)</sup>

* Message & Bodywork Magazine (Wayback Machine)
    * [The Anatomy of Communication, Part One (FebMar2007 PDF)](https://web.archive.org/web/2013*/http://www.massageandbodywork.com/Articles/FebMar2007/EssentialSkillsPart11.pdf)
    * [The Anatomy of Communication, Part II (AprMay2007 PDF)](https://web.archive.org/web/20110217033058/http://www.massageandbodywork.com/Articles/AprilMay2007/EssentialSkillsPart%202.pdf)
* MessageTherapy.com
    * [The Anatomy of Communication, Part One (HTML)](https://www.massagetherapy.com/articles/anatomy-communication-how-understanding-can-transform-our-professional-interactions) from Massage & Bodywork magazine, February/March 2007
* SAVI Communications: [The SAVI<sup>®</sup> Grid Official PDF versions](http://savicommunications.com/SAVI_grid.html) available for download.
* Wikipedia
    * [Systems-centered therapy](https://en.wikipedia.org/wiki/Systems-centered_therapy)

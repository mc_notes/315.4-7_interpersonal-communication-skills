# Notes: Anatomy of Communication

_Below are some thoughts on possible learning activities to help internalize the core concepts. This page is a work-in-progress._

Activities, Flashcard:

* Given: "some quote" or "A says. B says." dialog
    * Provide: Coding Square Name.
* Given: Coding Square Name
    * Provide: Square Name, Row Name, Column Name & Number
